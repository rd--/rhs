; all :: (a -> Bool) -> [a] -> Bool
;(define all
;  (lambda (f l)
;    (if (null? l)
;        #t
;        (and (f (car l)) (all f (cdr l))))))

; and :: [Bool] -> Bool
;(define allTrue
;  (lambda (l)
;    (if (null? l)
;        #t
;        (and (car l) (allTrue (cdr l))))))

; any :: (a -> Bool) -> [a] -> Bool
;(define any
;  (lambda (f l)
;    (if (null? l)
;        #f
;        (or (f (car l)) (any f (cdr l))))))

; (++) :: [a] -> [a] -> [a]
;(define append
;  (lambda (a b)
;    (if (null? a)
;        b
;        (cons (car a) (append (cdr a) b)))))

;(define ++ append)

; break :: (a -> Bool) -> [a] -> ([a],[a])
;(define break (lambda (p l) (span (compose not p) l)))

; concat :: [[a]] -> [a]
;(define concat (lambda (l) (foldr append nil l)))

; concatMap :: (a -> [b]) -> [a] -> [b]
;(define concatMap (lambda (f l) (concat (map f l))))

; deleteBy :: (a -> a -> Bool) -> a -> [a] -> [a]
;(define deleteBy
;  (lambda (f x l)
;    (if (null? l)
;        nil
;        (if (f x (car l))
;            (cdr l)
;            (cons (car l) (deleteBy f x (cdr l)))))))

; delete :: Eq a => a -> [a] -> [a]
;(define delete (lambda (x l) (deleteBy equal? x l)))

; drop :: Int -> [a] -> [a]
;(define drop
;  (lambda (n l)
;    (cond ((<= n 0) l)
;          ((null? l) nil)
;          (else (drop (- n 1) (cdr l))))))

; dropWhile :: (a -> Bool) -> [a] -> [a]
;(define dropWhile
;  (lambda (p l)
;    (if (null? l)
;        nil
;        (if (p (car l))
;            (dropWhile p (cdr l))
;            l))))

; elem :: Eq a => a -> [a] -> Bool
;(define elem (lambda (x l) (any (lambda (y) (equal? x y)) l)))

; elemIndex :: Eq a => a -> [a] -> Maybe Int
;(define elemIndex (lambda (x l) (findIndex (lambda (y) (equal? x y)) l)))

; elemIndices :: Eq a => a -> [a] -> [Int]
;(define elemIndices (lambda (x l) (findIndices (lambda (y) (equal? x y)) l)))

; find :: (a -> Bool) -> [a] -> Maybe a
;(define find
;  (lambda (f l)
;    (if (null? l)
;        #f
;        (if (f (car l))
;            (car l)
;            (find f (cdr l))))))

;(define findIndex*
;  (lambda (f l n)
;    (if (null? l)
;        #f
;        (if (f (car l))
;            n
;            (findIndex* f (cdr l) (+ n 1))))))

; findIndex :: (a -> Bool) -> [a] -> Maybe Int
;(define findIndex (lambda (f l) (findIndex* f l 0)))

;(define findIndices*
;  (lambda (f l n)
;    (if (null? l)
;        nil
;        (if (f (car l))
;            (cons n (findIndices* f (cdr l) (+ n 1)))
;            (findIndices* f (cdr l) (+ n 1))))))

; findIndices :: (a -> Bool) -> [a] -> [Int]
;(define findIndices (lambda (f l) (findIndices* f l 0)))

; filter :: (a -> Bool) -> [a] -> [a]
;(define filter
;  (lambda (f l)
;    (if (null? l)
;        nil
;        (let ((x (car l))
;              (xs (cdr l)))
;          (if (f x)
;              (cons x (filter f xs))
;              (filter f xs))))))

; foldl :: (a -> b -> a) -> a -> [b] -> a
;(define foldl
;  (lambda (f z l)
;    (if (null? l)
;        z
;        (foldl f (f z (car l)) (cdr l)))))

; foldl1 :: (a -> a -> a) -> [a] -> a
;(define foldl1 (lambda (f l) (foldl f (car l) (cdr l))))

; foldr :: (a -> b -> b) -> b -> [a] -> b
;(define foldr
;  (lambda (f z l)
;    (if (null? l)
;        z
;        (f (car l) (foldr f z (cdr l))))))

; foldr1 :: (a -> a -> a) -> [a] -> a
;(define foldr1
;  (lambda (f l)
;    (if (null? (cdr l))
;        (car l)
;        (f (car l) (foldr1 f (cdr l))))))

; groupBy :: (a -> a -> Bool) -> [a] -> [[a]]
;(define groupBy
;  (lambda (f l)
;    (if (null? l)
;        '()
;        (let* ((x (car l))
;               (yz (span (lambda (e) (f e x)) (cdr l))))
;          (cons (cons x (fst yz)) (groupBy f (snd yz)))))))

; init :: [a] -> [a]
;(define init
;  (lambda (l)
;    (let ((x (car l))
;          (xs (cdr l)))
;      (if (null? xs)
;          nil
;          (cons x (init xs))))))

; insert :: Ord a => a -> [a] -> [a]
;(define insert (lambda (e l) (insertBy compare e l)))

; insertBy :: (a -> a -> Ordering) -> a -> [a] -> [a]
;(define insertBy
;  (lambda (f x l)
;    (if (null? l)
;        (list1 x)
;        (if (equal? (f x (car l)) (quote gt))
;            (cons (car l) (insertBy f x (cdr l)))
;            (cons x l)))))

; intercalate :: [a] -> [[a]] -> [a]
;(define intercalate (lambda (e l) (concat (intersperse e l))))

; intersperse :: a -> [a] -> [a]
;(define intersperse
;  (lambda (x l)
;    (cond ((null? l) nil)
;          ((null? (cdr l)) l)
;          (else (cons (car l) (cons x (intersperse x (cdr l))))))))

; isInfixOf :: Eq a => [a] -> [a] -> Bool
;(define isInfixOf
;  (lambda (p q)
;    (cond ((null? p) #t)
;          ((null? q) #f)
;          (else (or (isPrefixOf p q)
;                    (isInfixOf p (cdr q)))))))

; isPrefixOf :: Eq a => [a] -> [a] -> Bool
;(define isPrefixOf
;  (lambda (p q)
;    (cond ((null? p) #t)
;          ((null? q) #f)
;          (else (and (equal? (car p) (car q))
;                     (isPrefixOf (cdr p) (cdr q)))))))

; isSuffixOf :: Eq a => [a] -> [a] -> Bool
;(define isSuffixOf (lambda (p q) (isPrefixOf (reverse p) (reverse q))))

; iterate :: (a -> a) -> a -> [a]
; iterateFinite n = take n . iterate :: Int -> (a -> a) -> a -> [a]
;(define iterate
;  (lambda (n f z)
;    (if (equal? n 0)
;        (list)
;        (cons z (iterate (- n 1) f (f z))))))

; last :: [a] -> a
;(define last
;  (lambda (l)
;    (let ((xs (cdr l)))
;      (if (null? xs)
;          (car l)
;          (last xs)))))

; length :: [a] -> Int
;(define length
;  (lambda (l)
;    (if (null? l)
;        0
;        (+ 1 (length (cdr l))))))

; listn :: a ... -> [a]
;(define list1 (lambda (x) (cons x nil)))
;(define list2 (lambda (x y) (cons x (cons y nil))))
;(define list3 (lambda (x y z) (cons x (cons y (cons z nil)))))
;(define list4 (lambda (x y z a) (cons x (cons y (cons z (cons a nil))))))
;(define list5 (lambda (x y z a b) (cons x (cons y (cons z (cons a (cons b nil)))))))

; (!!) :: [a] -> Int -> a
;(define !!
;  (lambda (l n)
;    (if (= n 0)
;        (car l)
;        (!! (cdr l) (- n 1)))))

; lookup :: Eq a => a -> [(a,b)] -> Maybe b
;(define lookup
;  (lambda (x l)
;    (if (null? l)
;        #f
;        (if (equal? (fst (car l)) x)
;            (snd (car l))
;            (lookup x (cdr l))))))

; map :: (a -> b) -> [a] -> [b]
;(define map
;  (lambda (f l)
;    (if (null? l)
;        nil
;        (cons (f (car l)) (map f (cdr l))))))

; mapAccumL :: (acc -> x -> (acc, y)) -> acc -> [x] -> (acc, [y])
;(define mapAccumL
;  (lambda (f s l)
;    (if (null? l)
;        (twoTuple s nil)
;        (let* ((a (f s (car l)))
;               (b (mapAccumL f (fst a) (cdr l))))
;          (twoTuple (fst b) (cons (snd a) (snd b)))))))

; mapAccumR :: (acc -> x -> (acc, y)) -> acc -> [x] -> (acc, [y])
;(define mapAccumR
;  (lambda (f s l)
;    (if (null? l)
;        (twoTuple s nil)
;        (let* ((a (mapAccumR f s (cdr l)))
;               (b (f (fst a) (car l))))
;          (twoTuple (fst b) (cons (snd b) (snd a)))))))

; maximum :: Ord a => [a] -> a
;(define maximum (lambda (l) (foldl1 max l)))

; minimum :: Ord a => [a] -> a
;(define minimum (lambda (l) (foldl1 min l)))

; nub :: Eq a => [a] -> [a]
;(define nub (lambda (l) (nubBy equal? l)))

; nubBy :: (a -> a -> Bool) -> [a] -> [a]
;(define nubBy
;  (lambda (f l)
;    (if (null? l)
;        nil
;        (let ((x (car l))
;              (xs (cdr l)))
;          (cons x (nubBy f (filter (lambda (y) (not (f x y))) xs)))))))

; nil :: [a]
;(define nil '())

; notElem :: Eq a => a -> [a] -> Bool
;(define notElem
;  (lambda (x l)
;    (all (lambda (y) (not (equal? x y))) l)))

; null :: [a] -> Bool
;(define null (lambda (x) (equal? x nil)))

; or :: [Bool] -> Bool
;(define anyTrue
;  (lambda (l)
;    (if (null? l)
;        #f
;        (or (car l) (anyTrue (cdr l))))))

; partition :: (a -> Bool) -> [a] -> ([a], [a])
;(define partition
;  (let ((select (lambda (p)
;                  (lambda (x tf)
;                    (let ((t (fst tf))
;                          (f (snd tf)))
;                      (if (p x)
;                          (twoTuple (cons x t) f)
;                          (twoTuple t (cons x f))))))))
;    (lambda (p xs)
;      (foldr (select p) (twoTuple nil nil) xs))))

; product :: Num a => [a] -> a
;(define product (lambda (l) (foldl * 1 l)))

; replicate :: Int -> a -> [a]
;(define replicate
;  (lambda (n x)
;    (if (= n 0)
;        nil
;        (cons x (replicate (- n 1) x)))))

; reverse :: [a] -> [a]
;(define reverse (lambda (l) (foldl (flip cons) nil l)))

; scanl :: (a -> b -> a) -> a -> [b] -> [a]
;(define scanl
;  (lambda (f q l)
;    (cons q (if (null? l)
;                nil
;                (scanl f (f q (car l)) (cdr l))))))

; scanl1 :: (a -> a -> a) -> [a] -> [a]
;(define scanl1
;  (lambda (f l)
;    (if (null? l)
;        nil
;        (scanl f (car l) (cdr l)))))

; scanr :: (a -> b -> b) -> b -> [a] -> [b]
;(define scanr
;  (lambda (f q0 l)
;    (if (null? l)
;        (list1 q0)
;        (let ((qs (scanr f q0 (cdr l))))
;          (cons (f (car l) (car qs)) qs)))))

; scanr1 :: (a -> a -> a) -> [a] -> [a]
;(define scanr1
;  (lambda (f l)
;    (if (null? l)
;        nil
;        (if (null? (cdr l))
;            l
;            (let ((qs (scanr1 f (cdr l))))
;              (cons (f (car l) (car qs)) qs))))))

; sort :: Ord a => [a] -> [a]
;(define sort (lambda (l) (sortBy compare l)))

; sortBy :: (a -> a -> Ordering) -> [a] -> [a]
;(define sortBy (lambda (f l) (mergeSort f l)))

; mergeSort :: (a -> a -> Ordering) -> [a] -> [a]
;(define mergeSort (lambda (f l) (mergeSort* f (map list1 l))))

; mergeSort' :: (a -> a -> Ordering) -> [[a]] -> [a]
;(define mergeSort*
;  (lambda (f l)
;    (cond ((null? l) nil)
;          ((null? (cdr l)) (car l))
;          (else (mergeSort* f (mergePairs f l))))))

; mergePairs :: (a -> a -> Ordering) -> [[a]] -> [[a]]
;(define mergePairs
;  (lambda (f l)
;    (cond ((null? l) nil)
;          ((null? (cdr l)) l)
;          (else (cons (merge f (car l) (car (cdr l)))
;                      (mergePairs f (cdr (cdr l))))))))

; merge :: (a -> a -> Ordering) -> [a] -> [a] -> [a]
;(define merge
;  (lambda (f l r)
;    (cond ((null? l) r)
;          ((null? r) l)
;          (else (if (equal? (f (car l) (car r)) (quote gt))
;                    (cons (car r) (merge f l (cdr r)))
;                    (cons (car l) (merge f (cdr l) r)))))))

; span :: (a -> Bool) -> [a] -> ([a],[a])
;(define span
;  (lambda (p l)
;    (if (null? l)
;        (twoTuple nil nil)
;        (if (p (car l))
;            (let ((r (span p (cdr l))))
;              (twoTuple (cons (car l) (fst r)) (snd r)))
;            (twoTuple nil l)))))

; splitAt :: Int -> [a] -> ([a],[a])
;(define splitAt (lambda (n l) (twoTuple (take n l) (drop n l))))

; sum :: Num a => [a] -> a
;(define sum (lambda (l) (foldl + 0 l)))

; take :: Int -> [a] -> [a]
;(define take
;  (lambda (n l)
;    (cond ((<= n 0) nil)
;          ((null? l) nil)
;          (else (cons (car l) (take (- n 1) (cdr l)))))))

; takeWhile :: (a -> Bool) -> [a] -> [a]
;(define takeWhile
;  (lambda (p l)
;    (if (null? l)
;        nil
;        (if (p (car l))
;            (cons (car l) (takeWhile p (cdr l)))
;            nil))))

; transpose :: [[a]] -> [[a]]
;(define transpose
;  (lambda (l)
;    (let ((protect
;           (lambda (f)
;             (lambda (x)
;               (if (null? x)
;                   nil
;                   (f x))))))
;      (cond ((null? l) nil)
;            ((null? (car l)) (transpose (cdr l)))
;            (else (let* ((e (car l))
;                         (x (car e))
;                         (xs (cdr e))
;                         (xss (cdr l)))
;                    (cons (cons x
;                                (filter (compose not null?)
;                                        (map (protect car) xss)))
;                          (transpose (cons xs
;                                           (map (protect cdr) xss))))))))))

; unfoldr :: (b -> Maybe (a, b)) -> b -> [a]
;(define unfoldr
;  (lambda (f x)
;    (let ((r (f x)))
;      (if r
;          (cons (fst r) (unfoldr f (snd r)))
;          nil))))


; union :: Eq a => [a] -> [a] -> [a]
;(define union (lambda (a b) (unionBy equal? a b)))

; unionBy :: (a -> a -> Bool) -> [a] -> [a] -> [a]
;(define unionBy
;  (lambda (f xs ys)
;    (let ((g (lambda (x y) (deleteBy f y x))))
;      (append xs (foldl g (nubBy f ys) xs)))))

; zip :: [a] -> [b] -> [(a,b)]
;(define zip
;  (lambda (a b)
;    (zipWith twoTuple a b)))

; zip3 :: [a] -> [b] -> [c] -> [(a,b,c)]
;(define zip3
;  (lambda (a b c)
;    (zipWith3 (lambda (p q r) (threeTuple p q r)) a b c)))

; zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
;(define zipWith
;  (lambda (f a b)
;    (cond ((null? a) nil)
;          ((null? b) nil)
;          (else (cons (f (car a) (car b))
;                      (zipWith f (cdr a) (cdr b)))))))

; zipWith3 :: (a -> b -> c -> d) -> [a] -> [b] -> [c] -> [d]
;(define zipWith3
;  (lambda (f a b c)
;    (cond ((null? a) nil)
;          ((null? b) nil)
;          ((null? c) nil)
;          (else (cons (f (car a) (car b) (car c))
;                      (zipWith3 f (cdr a) (cdr b) (cdr c)))))))
