; fst :: (a, b) -> a
;(define fst (lambda (x) (vector-ref x 0)))

; snd :: (a, b) -> b
;(define snd (lambda (x) (vector-ref x 1)))

; curry :: ((a, b) -> c) -> a -> b -> c
;(define curry
;  (lambda (f)
;    (lambda (x y)
;      (f (vector x y)))))

; uncurry :: (a -> b -> c) -> (a, b) -> c
;(define uncurry
;  (lambda (f)
;    (lambda (c)
;      (f (fst c) (snd c)))))

; swap :: (a, b) -> (b, a)
;(define swap
;  (lambda (x)
;    (twoTuple (snd x) (fst x))))

; (,) :: a -> b -> (a, b)
;(define twoTuple
;  (lambda (p q)
;    (vector p q)))

; (,,) :: a -> b -> c -> (a, b, c)
;(define threeTuple
;  (lambda (p q r)
;    (vector p q r)))
