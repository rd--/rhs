; replicateM :: Applicative m => Int -> m a -> m [a]
; (define replicateM
;   (lambda (i x)
;     (if (<= i 0)
;         '()
;         (cons (x) (replicateM (- i 1) x)))))
