; undefined :: a
;(define undefined
;  (lambda ()
;    (error "undefined" "undefined")))

;;;; class Eq

; (==) :: Eq a => a -> a -> Bool
;(define == equal?)

; (/=) :: Eq a => a -> a -> Bool
;(define /=
;  (lambda (x y)
;    (not (equal? x y))))

;;;; class Num

; negate :: Num a => a -> a
;(define negate
;  (lambda (n)
;    (- n)))

; signum :: Num a => a -> a
;(define signum
;  (lambda (x)
;    (cond ((> x 0) 1)
;          ((< x 0) -1)
;          (else 0))))

; even :: Integral a => a -> Bool
;(define even even?)

; odd :: Integral a => a -> Bool
;(define odd odd?)

;;;; class Enum

; pred :: Enum a => a -> a
;(define pred (lambda (x) (- x 1)))

; succ :: Enum a => a -> a
;(define succ (lambda (x) (+ x 1)))

;(define enumFromDifferenceTo
;  (lambda (f i x k)
;    (cond ((= i k) (list1 k))
;          ((f i k) nil)
;          (else (cons i (enumFromDifferenceTo f (+ i x) x k))))))

; enumFromThenTo :: Enum a => a -> a -> a -> [a]
;(define enumFromThenTo
;  (lambda (i j k)
;    (let ((x (- j i)))
;      (enumFromDifferenceTo (if (> x 0) > <) i x k))))

; enumFromTo :: Enum a => a -> a -> [a]
;(define enumFromTo
;  (lambda (i j)
;    (enumFromThenTo i (succ i) j)))
