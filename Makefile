all:
	(cd src; make all)
	(cd mk; make all)

clean:
	(cd src; make clean)
	(cd mk; make clean)

push-all:
	r.gitlab-push.sh rhs
