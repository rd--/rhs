--even n = rem n 2 == 0

--odd = not . even

negate x = 0 - x

rem = remainder

quot = quotient

--mod = modulo -- builtin

undefined _ = error "undefined" "undefined"

(/=) x y = not (x == y)

signum x = if x > 0 then 1 else if x < 0 then -1 else 0

pred x = x - 1

succ x = x + 1

enumFromDifferenceTo f i x k = if i == k then [k] else if f i k then [] else i : enumFromDifferenceTo f (i + x) x k

enumFromThenTo i j k = let x = j - i in enumFromDifferenceTo (if x > 0 then (>) else (<)) i x k

enumFromTo i j = enumFromThenTo i (succ i) j
