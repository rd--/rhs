(define negate (lambda (x) (- 0 x)))
(define rem remainder)
(define quot quotient)
(define undefined (lambda (_) (error "undefined" "undefined")))
(define /= (lambda (x y) (not (equal? x y))))
(define signum (lambda (x) (if (> x 0) 1 (if (< x 0) -1 0))))
(define pred (lambda (x) (- x 1)))
(define succ (lambda (x) (+ x 1)))
(define enumFromDifferenceTo (lambda (f i x k) (if (equal? i k) (list k) (if (f i k) (quote ()) (cons i (enumFromDifferenceTo f (+ i x) x k))))))
(define enumFromThenTo (lambda (i j k) (letrec ((x (- j i))) (enumFromDifferenceTo (if (> x 0) > <) i x k))))
(define enumFromTo (lambda (i j) (enumFromThenTo i (succ i) j)))
