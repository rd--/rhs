twoTuple x y = (x, y)

threeTuple x y z = (x, y, z)

fst x = vectorRef x 0

snd x = vectorRef x 1

curry f = \x y -> f (x,y)

uncurry f = \c -> f (fst c) (snd c)

swap x = (snd x,fst x)
