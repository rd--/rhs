(define && (lambda (p q) (and p q)))
(define otherwise #t)
(define ifThenElse (lambda (p q r) (if p (q) (r))))
