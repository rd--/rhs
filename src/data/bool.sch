(&&) p q = and p q

-- (||) p q = or p q -- |symbol| syntax is invalid in #!r6rs mode

otherwise = True

-- not x = if x == False then True else False

ifThenElse p q r = if p then q () else r ()
