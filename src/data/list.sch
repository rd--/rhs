-- (:) p q = cons p q

-- (++) p q = append p q -- invalid numeric sequence

head = car

tail = cdr

(!!) l n = if n == 0 then head l else tail l !! (n - 1)

all f l = if null l then True else f (head l) && all f (tail l)

allTrue l = if null l then True else head l && allTrue (tail l)

any f l = if null l then False else f (head l) || any f (tail l)

anyTrue l = if null l then False else head l || anyTrue (tail l)

--append a b = if null a then b else head a : append (tail a) b

break p l = span (not . p) l

concat l = foldr append [] l

concatMap f l = concat (map f l)

deleteBy f x l =
  if null l
  then []
  else if f x (head l)
       then tail l
       else head l : deleteBy f x (tail l)

delete x l = deleteBy (==) x l

drop n l = if n <= 0 then l else if null l then [] else drop (n - 1) (tail l)

dropWhile p l = if null l then [] else if p (head l) then dropWhile p (tail l) else l

elem x l = any (\y -> x == y) l

elemIndex x l = findIndex (\y -> x == y) l

elemIndices x l = findIndices (\y -> x == y) l

-- find f l = if null l then Nothing else if f (head l) then Just (head l) else find f (tail l)

findIndexStar f l n =
  if null l
  then False
  else if f (head l)
       then n
       else findIndexStar f (tail l) (n + 1)

findIndex f l = findIndexStar f l 0

findIndicesStar f l n =
  if null l
  then []
  else if f (head l)
       then n : findIndicesStar f (tail l) (n + 1)
       else findIndicesStar f (tail l) (n + 1)

findIndices f l = findIndicesStar f l 0

filter f l =
  if null l
  then []
  else let x = head l
           xs = tail l
       in if f x then x : filter f xs else filter f xs

foldl f z l = if null l then z else foldl f (f z (head l)) (tail l)

foldl1 f l = foldl f (head l) (tail l)

foldr f z l = if null l then z else f (head l) (foldr f z (tail l))

foldr1 f l = if null (tail l) then head l else f (head l) (foldr1 f (tail l))

groupBy f l =
  if null l
  then []
  else let x = head l
           yz = span (\e -> f e x) (tail l)
       in x : head yz : groupBy f (tail yz)

init l =
  let x = head l
      xs = tail l
  in if null xs then [] else x : init xs

insert e l = insertBy compare e l

insertBy f x l =
  if null l
  then [x]
  else if f x (head l) == quote gt
       then head l : insertBy f x (tail l)
       else x : l

intercalate e l = concat (intersperse e l)

intersperse x l =
  if null l
  then []
  else if null (tail l)
       then l
       else head l : x : intersperse x (tail l)

isInfixOf p q =
  if null p
  then True
  else if null q
       then False
       else isPrefixOf p q || isInfixOf p (tail q)

isPrefixOf p q =
  if null p
  then True
  else if null q
       then False
       else (head p == head q) && isPrefixOf (tail p) (tail q)

isSuffixOf p q = isPrefixOf (reverse p) (reverse q)

iterate n f z = if n == 0 then [] else z : iterate (n - 1) f (f z)

last l = let xs = tail l in if null xs then head l else last xs

--length l = if null l then 0 else 1 + length (tail l)

lookup x l =
  if null l
  then False
  else if fst (head l) == x
       then snd (head l)
       else lookup x (tail l)

--map f l = if null l then [] else f (head l) : map f (tail l)

mapAccumL f s l=
  if null l
  then (s,[])
  else let a = f s (head l)
           b = mapAccumL f (fst a) (tail l)
       in (fst b,snd a : snd b)

mapAccumR f s l =
  if null l
  then (s,[])
  else let a = mapAccumR f s (tail l)
           b = f (fst a) (head l)
       in (fst b,snd b : snd a)

maximum l = foldl1 max l

minimum l = foldl1 min l

merge f l r =
  if null l
  then r
  else if null r
       then l
       else if f (head l) (head r) == quote gt
            then head r : merge f l (tail r)
            else head l : merge f (tail l) r

mergePairs f l =
  if null l
  then []
  else if null (tail l)
       then l
       else merge f (head l) (head (tail l)) : mergePairs f (tail (tail l))

mergeSort f l = mergeSortStar f (map list l)

mergeSortStar f l =
  if null l
  then []
  else if null (tail l)
       then head l
       else mergeSortStar f (mergePairs f l)

nil = []

notElem x l = all (\y -> not (x == y)) l

nub l = nubBy (==) l

nubBy f l =
  if null l
  then []
  else let x = head l
           xs = tail l
       in x : nubBy f (filter (\y -> not (f x y)) xs)

-- null x = x == []

partitionFoldr p xs =
  let select = \p -> \x tf -> let t = fst tf
                                  f = snd tf
                              in if p x then (x : t,f) else (t,x : f)
  in foldr (select p) ([],[]) xs

product l = foldl (*) 1 l

replicate n x = if n == 0 then [] else x : replicate (n - 1) x

--reverse l = foldl (flip (:)) [] l

scanl f q l = q : (if null l then [] else scanl f (f q (head l)) (tail l))

scanl1 f l = if null l then [] else scanl f (head l) (tail l)

scanr f q0 l =
  if null l
  then [q0]
  else let qs = scanr f q0 (tail l)
       in f (head l) (head qs) : qs

scanr1 f l =
  if null l
  then []
  else if null (tail l)
       then l
       else let qs = scanr1 f (tail l)
            in f (head l) (head qs) : qs

sort l = sortBy compare l

sortBy f l = mergeSort f l

span p l =
  if null l
  then ([],[])
  else if p (head l)
       then let r = span p (tail l)
            in (head l : fst r,snd r)
       else ([],l)

splitAt n l = (take n l,drop n l)

sum l = foldl (+) 0 l

take n l =
  if n <= 0
  then []
  else if null l
       then []
       else head l : take (n - 1) (tail l)

takeWhile p l =
  if null l
  then []
  else if p (head l)
       then head l : takeWhile p (tail l)
       else []

transpose l =
  let protect f = \x -> if null x then [] else f x
  in if null l
     then []
     else if null (head l)
          then transpose (tail l)
          else let e = head l
                   x = head e
                   xs = tail e
                   xss = tail l
                   lhs = x : filter (not . null) (map (protect head) xss)
                   rhs = transpose (xs : map (protect tail) xss)
               in lhs : rhs

unfoldr f x = let r = f x in if isJust r then fst (fromJust r) : unfoldr f (snd (fromJust r)) else []

union a b = unionBy (==) a b

unionBy f xs ys =
  let g x y = deleteBy f y x
  in xs ++ foldl g (nubBy f ys) xs

zip a b = zipWith twoTuple a b

zip3 a b c = zipWith3 threeTuple a b c

zipWith f a b =
  if null a || null b
  then []
  else f (head a) (head b) : zipWith f (tail a) (tail b)

zipWith3 f a b c =
  if null a || null b || null c
  then []
  else f (head a) (head b) (head c) : zipWith3 f (tail a) (tail b) (tail c)
