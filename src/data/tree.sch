flattenWorker t r =
  if null t
  then r
  else if isPair t
       then flattenWorker (head t) (flattenWorker (tail t) r)
       else t : r

flatten t = flattenWorker t []

levels t =
  if null t
  then []
  else let lr = partitionFoldr (not . isPair) t
       in fst lr : levels (concat (snd lr))

treeMap f l = if isList l then map (\e -> treeMap f e) l else f l
