(define flattenWorker (lambda (t r) (if (null? t) r (if (pair? t) (flattenWorker (head t) (flattenWorker (tail t) r)) (cons t r)))))
(define flatten (lambda (t) (flattenWorker t (quote ()))))
(define levels (lambda (t) (if (null? t) (quote ()) (letrec ((lr (partitionFoldr (compose not pair?) t))) (cons (fst lr) (levels (concat (snd lr))))))))
(define treeMap (lambda (f l) (if (isList l) (map (lambda (e) (treeMap f e)) l) (f l))))
