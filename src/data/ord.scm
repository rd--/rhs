(define compare (lambda (x y) (if (> x y) (quote gt) (if (< x y) (quote lt) (quote eq)))))
