-- (.) f g = \x -> f (g x) -- invalid dot while reading list

compose f g = \x -> f (g x)

const x = \_ -> x

flip f = \x y -> f y x

id x = x

on j f = \x y -> j (f x) (f y)
