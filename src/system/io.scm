(define hGetContents (lambda (fd) (letrec ((f (lambda (r) (letrec ((c (readChar fd))) (if (isEofObject c) (listToString (reverse r)) (f (cons c r))))))) (f (quote ())))))
(define readFile (lambda (fn) (callWithInputFile fn (lambda (p) (hGetContents p)))))
(define putStrLn (lambda (str) (>> (display str) (newline))))
