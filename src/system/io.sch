-- port -> String
hGetContents fd =
  let f r =
        let c = readChar fd
        in if isEofObject c then listToString (reverse r) else f (cons c r)
  in f []

-- filepath -> string
readFile fn = callWithInputFile fn (\p -> hGetContents p)

-- string -> ()
putStrLn str = display str >> newline ()
