; alias
(define == isEqual)
(define even isEven)
(define isBool isBoolean)
(define isInt isInteger)
(define isLower isCharLowerCase)
(define isUpper isCharUpperCase)
(define odd isOdd)
(define toLower charDowncase)
(define toUpper charUpcase)
