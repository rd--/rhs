(define >> (lambda (p q) (begin p q)))
(define replicateM (lambda (i x) (if (<= i 0) (quote ()) (cons (x) (replicateM (- i 1) x)))))
