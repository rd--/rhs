(>>) p q = begin p q

replicateM i x = if i <= 0 then [] else x () : replicateM (i - 1) x
