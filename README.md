rhs
---

relevant haskell libraries in scheme

a
[scheme](http://library.readscheme.org/standards.html)
implementation of parts of the
[haskell](http://haskell.org/)
hierarchical libraries.

~~~~
haskell             rhs                 rnrs
------------------- ------------------- --------------------
Control.Monad
replicateM          replicateM
                    _replicateM

Data.Bool
(&&)                and2                and
                    and3
(||)                or2                 or
                    or3
otherwise           otherwise
not                 not                 not

Data.Function
(.)                 compose             compose
const               const
flip                flip
id                  id

Data.List           list.scm
all                 all
and                 allTrue
any                 anyTrue
(++)                append2             append
break               break
concat              concat
concatMap           concatMap
deleteBy            deleteBy
delete              delete
dropWhile           dropWhile
elem                elem                member
elemIndex           elemIndex
find                find
findIndex           findIndex
findIndices         findIndices
filter              filter              filter
foldl               foldl               fold-left
foldl1              foldl1
foldr               foldr               fold-right
foldr1              foldr1
groupBy             groupBy
head                head                car
init                init
insert              insert
insertBy            insertBy
intercalate         intercalate
intersperse         intersperse
isInfixOf           isInfixOf
isPrefixOf          isPrefixOf
isSuffixOf          isSuffixOf
last                last
length              length
[,]                 list
                    list[1,2,3,4,5]
(!!)                !!                  list-ref
lookup              lookup
map                 map1                map
mapAccumL           mapAccumL
mapAccumR           mapAccumR
maximum             maximum
minimum             minimum
nub                 nub
nubBy               nubBy
[]                  nil
notElem             notElem
null                null                null?
or                  anyTrue
partition           partition*          partition
product             product
replicate           replicate
reverse             reverse             reverse
scanl               scanl
scanll              scanl1
scanr               scanr
scanr1              scanr1
sort                sort                list-sort
sortBy              sortBy
span                span
splitAt             splitAt
sum                 sum
tail                tail                cdr
take                take
takeWhile           takeWhile
transpose           transpose
unfoldr             unfoldr
union               union
unionBy             union-by
zip                 zip
zip3                zip3
zipWith             zipWith             map
zipWith3            zipWith3            map

Data.Ord
LT, EQ, GT          'lt, 'eq, 'gt
compare             compare
max                 max2                max
min                 min2                min

Data.String
lines               lines

Data.Tree
flatten             flatten
levels              levels

Data.Tuple
curry               curry
fst                 fst                 vector-ref _ 0
snd                 snd                 vector-ref _ 1
(,)                 twoTuple            vector
(,,)                threeTuple          vector
uncurry             uncurry

Prelude             prelude.scm
enumFromThenTo      enum-from-then-to
enumFromTo          enum-from-to
error                                   error
even                even                even?
odd                 odd                 odd?
pred                pred
signum              signum
succ                succ

System.IO           system/io.scm
readFile            readFile
~~~~

Haskell:

- [Control.Monad](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Control-Monad.html)
- [Data.Bool](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Data-Bool.html)
- [Data.Function](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Data-Function.html)
- [Data.List](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Data-List.html)
- [Data.Ord](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Data-Ord.html)
- [Data.Tree](http://hackage.haskell.org/packages/archive/containers/latest/doc/html/Data-Tree.html)
- [Data.Tuple](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Data-Tuple.html)
- [Prelude](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Prelude.html)

Scheme:

- [data/list.scm](?t=rhs&e=src/data/list.scm)
- [prelude.scm](?t=rhs&e=src/prelude.scm)

Tested with:
chezscheme-9.5.4,
ikarus-0.0.4-1870,
guile-3.0.4

Requires:
[hsc3-lisp](https://gitlab.com/rd--/hsc3-lisp)

© [rohan drape](http://rohandrape.net/),
  2008-2022,
  [gpl](http://gnu.org/copyleft/)
