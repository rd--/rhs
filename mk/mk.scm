(import (rnrs) (mk-r6rs core))

; [string]
(define rhs-file-seq
  (read-file-lines "rhs-file-seq.text"))

(define rhs-src
  (map (lambda (x) (string-append "../" x)) rhs-file-seq))

; bindings rhs introduces but ought not to export
(define rhs-private
  (quote
   (mergeSort mergeSortStar mergePairs merge)))

; equalities with the (rnrs) library
(define rnrs-equalities
  '(append
    filter
    find
    length
    map
    min max
    not
    null?
    partition ; multiple values
    reverse))

(define lib-dir (list-ref (command-line) 1))

; r6rs
(mk-r6rs '(rhs core)
         rhs-src
         (string-append lib-dir "/r6rs/rhs/core.sls")
         '((rnrs base)  (rnrs io simple) (rnrs mutable-pairs) (rnrs mutable-strings) (rnrs unicode) (rnrs r5rs))
         rhs-private
         rnrs-equalities)

; racket
;(mk-racket 'rhs
;           rhs-src
;           (string-append lib-dir "/racket/rhs/main.rkt")
;           '(racket/base)
;           rhs-private
;           rnrs-equalities)

(exit)
