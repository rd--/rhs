; r6rs
(import (rnrs) (rhs core))

;;;; Data.List

;(== (list1 1) '(1)) ; #t
;(== (list2 1 2) '(1 2)) ; #t
;(== (list3 1 2 3) '(1 2 3)) ; #t
;(== (list4 1 2 3 4) '(1 2 3 4)) ; #t
;(== (list5 1 2 3 4 5) '(1 2 3 4 5)) ; #t

(let ((l (cons 1 (cons 2 (cons 3 '())))))
  (list
   (== l '(1 2 3))
   (== (not (null? l)) #t)
   (== (pair? l) #t)
   (== (list? l) #t)
   (== (length l) 3)
   (== (append l l) '(1 2 3 1 2 3)))) ; (#t #t #t #t #t #t)

(== (append '(1 2 3) (list 4 5 6)) '(1 2 3 4 5 6)) ; #t
(== (foldl + 0 (list 1 2 3)) 6) ; #t
(== (foldl (flip cons) '() (list 1 2 3)) '(3 2 1)) ; #t
(== (foldr cons '() (list 1 2 3)) '(1 2 3)) ; #t

(== (list 1 2 ((lambda (_) (cons 3 4)) nil) 5) '(1 2 (3 . 4) 5)) ; #t

(== (maximum '(1 3 5 4 2 0)) 5) ; #t
(== (minimum '(1 3 5 4 2 0)) 0) ; #t
(maximum '()) ; error

(== nil '()) ; #t

(== (filter even '(1 2 3 4 5 6)) '(2 4 6)) ; #t
(== (filter odd? (list 1 2 3 4 5 6)) '(1 3 5)) ; #t

(== (nub '(1 2 2 3 3 3)) '(1 2 3)) ; #t
(== (nubBy (on equal? car) '((0 1) (0 2) (1 2))) '((0 1) (1 2))) ; #t

(notElem 1 '(0 2 4)) ; #t

(== (reverse (list 1 2 3)) '(3 2 1)) ; #t

(== (let ((x (twoTuple 1 2))) (twoTuple (snd x) (fst x))) (twoTuple 2 1)) ; #t
(== (twoTuple 1 2) (swap (twoTuple 2 1))) ; #t

; mapAccumL (\st x -> let r = st + x in (r,(x,r))) 0 [1,3,5,7,9])
(==
 (mapAccumL (lambda (st x) (let ((r (+ st x))) (twoTuple r (cons x r)))) 0 '(1 3 5 7 9))
 '#(25 ((1 . 1) (3 . 4) (5 . 9) (7 . 16) (9 . 25)))) ; #t

; mapAccumR (\st x -> let r = st + x in (r,(x,r))) 0 [1,3,5,7,9]
(==
 (mapAccumR (lambda (st x) (let ((r (+ st x))) (twoTuple r (cons x r)))) 0 '(1 3 5 7 9))
 '#(25 ((1 . 25) (3 . 24) (5 . 21) (7 . 16) (9 . 9)))) ; #t

(== (iterate 8 (lambda (n) (* n 2)) 1) (list 1 2 4 8 16 32 64 128)) ; #t

(== (transpose '((1 2 3) (4 5 6) (a b c) (e f g))) '((1 4 a e) (2 5 b f) (3 6 c g))) ; #t

(== (unfoldr (lambda (b) (if (= b 0) #f (twoTuple b (- b 1)))) 10) '(10 9 8 7 6 5 4 3 2 1)) ; #t

(== (!! (enumFromTo 1 9) 7) 8) ; #t

(== (zip (list 1 2 3) (list 4 5 6)) '(#(1 4) #(2 5) #(3 6))) ; #t
(== (zip3 (list 1 2 3) (list 4 5 6) (list 7 8 9)) '(#(1 4 7) #(2 5 8) #(3 6 9))) ; #t
(== (zipWith3 list '(a b c) '(d e f) '(h i j)) '((a d h) (b e i) (c f j))) ; #t

(== (break (lambda (x) (> x 3)) (list 1 2 3 4 5)) '#((1 2 3) (4 5))) ; #t
(== (span (lambda (x) (< x 3)) (list 1 2 3 4 5)) '#((1 2) (3 4 5))) ; #t

(== (lookup 3 (list '#(1 2) '#(2 3) '#(3 4))) 4) ; #t

(== (partitionFoldr even '(1 2 3 4 5)) '#((2 4) (1 3 5))) ; #t
(== (partitionFoldr (lambda (x) (elem x (list 2 4))) (list 1 2 3 4 5)) (twoTuple (list 2 4) (list 1 3 5))) ; #t

;;;; Data.Tree

(== (flatten '(1 2 (3 4 (5)) 6)) '(1 2 3 4 5 6)) ; #t
(== (levels '(1 2 (3 4 (5)) 6)) '((1 2 6) (3 4) (5))) ; #t

;;;; Prelude

(== (negate 3) -3) ; #t
(== (map signum '(-3 0 3)) '(-1 0 1)) ; #t

(== (enumFromDifferenceTo > 0 1 9) '(0 1 2 3 4 5 6 7 8 9)) ; #t
(== (enumFromDifferenceTo < 0 -3 -9) '(0 -3 -6 -9)) ; #t
(== (enumFromThenTo 0 1 9) '(0 1 2 3 4 5 6 7 8 9)) ; #t
(== (enumFromThenTo 0 -3 -9) '(0 -3 -6 -9)) ; #t
(== (enumFromTo 0 9) '(0 1 2 3 4 5 6 7 8 9)) ; #t
(== (succ 1) 2) ; #t
(== (pred 2) 1) ; #t

;;;; Control.Monad

(replicateM 4 (lambda () (begin (display 'r) (newline)))) ; r\nr\nr\nr\n

;;;; Control.Monad (syntax)

(_replicateM 4 (begin (display 'r) (newline))) ; r\nr\nr\nr\n

;;;; Data.Function

(== ((compose succ pred) 3) 3) ; #t
(== ((compose pred pred) 3) 1) ; #t
(== ((const 1) 2) 1) ; #t
(== ((flip -) 1 2) 1) ; #t
(== ((flip append) '(1 2 3) '(4 5 6)) '(4 5 6 1 2 3)) ; #t
(== (id 1) 1) ; #t

;;;; Data.Bool

(== otherwise #t) ; #t
(== (not #f) #t) ; #t
(ifThenElse true (lambda () (putStrLn "true")) (lambda () (putStrLn "false")))

;;;; Data.Bool (syntax)

(== (_and2 #f (undefined)) #f) ; #t
(== (_or2 #t (undefined)) #t) ; #t

;;;; Data.Tuple

(== (fst (twoTuple 'a 'b)) 'a) ; #t
(== (snd '#(a b)) 'b) ; #t
(== (threeTuple 'a 'b 'c) '#(a b c)) ; #t
(== ((curry (lambda (x) (list (fst x) (snd x)))) 1 2) '(1 2)) ; #t
(== ((uncurry (lambda (x y) (list x y))) (twoTuple 1 2)) '(1 2)) ; #t
(== (swap '#(a b)) '#(b a)) ; #t

(== (substring (readFile "/home/rohan/sw/rsc3/README.md") 0 4) "rsc3") ; #t

(isInteger 0) ; #t
(isInteger 0.0) ; #t
(isReal 0) ; #t
(isReal 0.0) ; #t
(isExact 0) ; #t
(isExact 0.0) ; #f
