all even [2,4,6,8] == True
all odd [1,3,5,7] == True
all Data.Char.isLower "string" == True

any even [1,2,3,4,5] == True
any Data.Char.isUpper "string" == False

break (> 3) [1,2,3,4,5] == ([1,2,3],[4,5])

concat [[1,2,3],[4,5]] == [1,2,3,4,5]

concatMap (\x -> [x,x]) [1,2,3] == [1,1,2,2,3,3]

drop 2 [1,2,3,4,5] == [3,4,5]
drop 2 "string" == "ring"

dropWhile (< 3) [1,2,3,4,5] == [3,4,5]

elem 3 [1,2,3,4,5] == True

find (== 3) [1,2,3,4,5] == Just 3

foldl (+) 0 [1,2,3,4,5] == 15
foldl (-) 0 [1,2,3,4,5] == -15

foldr (+) 0 [1,2,3,4,5] == 15
foldr (-) 0 [1,2,3,4,5] == 3 -- 0 - (1 - (2 - (3 - (4 - 5))))

iterate 8 (* 2) 1 == [1,2,4,8,16,32,64,128]

last [1,2,3,4,5] == 5

length [1,2,3,4,5] == 5
length "string" == 6

head [1,2,3,4,5] == 1

map (* 2) [1,2,3,4,5] == [2,4,6,8,10]

maximum [2,4,1,3] == 4

minimum [2,4,1,3] == 1

notElem 3 [1,2,3,4,5] == False

null [] == True
null "" == True
null [1,2,3,4,5] == False

product [1,2,3,4,5] == 120

replicate 5 1 == [1,1,1,1,1]

reverse [1,2,3,4,5] == [5,4,3,2,1]

scanl (+) 1 [1,2,3,4,5] == [1,2,4,7,11,16]

splitAt 3 [1,2,3,4,5] == ([1,2,3],[4,5])

sum [1,2,3,4,5] == 15

unfoldr (\b -> if b == 0 then Nothing else Just (b, b-1)) 10 == [10,9,8,7,6,5,4,3,2,1]
